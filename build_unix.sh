#!/usr/bin/env bash

# Define directories
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
LIB_DIR=$SCRIPT_DIR/lib
DIST_DIR=$SCRIPT_DIR/dist
BUILD_DIR=$SCRIPT_DIR/build

# Clean dist & cache before build
rm -rf $DIST_DIR
rm -rf $BUILD_DIR
rm -rf $SCRIPT_DIR/DonatePayMusicToObs.spec

# Build design
pyuic5 $LIB_DIR/design.ui -o $LIB_DIR/design.py

# Build application to a single file
pyinstaller --clean --onefile \
    --windowed --noconsole \
    --name="DonatePayMusicToObs" \
    --icon $LIB_DIR/icon.ico \
    --add-binary="$LIB_DIR/icon.ico:lib" \
    main.py

# Move config file to dist directory
cp config.json $DIST_DIR/