import sys

import logging
from obswebsocket import obsws, requests
logging.basicConfig(level=logging.INFO)

sys.path.append('../')


class ObsRemote:
    def __init__(self, host='localhost', port=4444, password=''):
        self.ws = None
        self.connected = False
        self.host = host
        self.port = port
        self.password = password

    def connect(self):
        self.ws = obsws(self.host, self.port, self.password)
        self.ws.connect()
        self.connected = True

    def disconnect(self):
        self.ws.disconnect()
        self.connected = False

    def is_connected(self):
        return self.connected

    def switch_to_scene(self, scene_name=''):
        self.ws.call(requests.SetCurrentScene(scene_name))
