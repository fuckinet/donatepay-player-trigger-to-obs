import requests
import re
import json
import os
import sys


if getattr(sys, 'frozen', False):
    path = os.path.dirname(sys.executable)
else:
    path = os.path.dirname(os.path.abspath(__file__))


def parse_config_file():
    import json
    with open(os.path.join(path, 'config.json')) as f:
        config_data = json.load(f)
    return config_data


def parse_cookies_dict_to_string(cookies_dict):
    return "; ".join([str(x) + "=" + str(y) for x, y in cookies_dict.items()])


def parse_socket_url_from_content(content):
    content = str(content)
    result = re.search("getCentrConf\(data\){return{url:'http:\/\/([A-z0-9.:]*)',", content).span()
    content = content[result[0]:result[1]]
    # parse again
    result = re.search("([A-z0-9.:]{13,32})", content).span()
    content = content[result[0]:result[1]].split(':')
    return {
        'host': content[0],
        'port': content[1]
    }


def parse_csrf_token_from_content(content):
    content = str(content)
    result = re.search("functiongetCSRF\(\){return'([A-z0-9]*)';\}", content).span()
    content = content[result[0]:result[1]]
    # parse again
    result = re.search("([A-z0-9]{24,60})", content).span()
    content = content[result[0]:result[1]]
    return content


def parse_user_id_from_content(content):
    content = str(content)
    result = re.search("functiongetUserId\(\){return'([0-9]*)';\}", content).span()
    content = content[result[0]:result[1]]
    # parse again
    result = re.search("([0-9]{1,12})", content).span()
    content = content[result[0]:result[1]]
    return content


def get_csrf_token_and_cookies_and_socket_url(user_token):
    url = 'http://widget.donatepay.ru/player/widget/' + str(user_token)
    res = requests.get(url, stream=False)
    cookies = res.cookies.get_dict()
    res_clean = res.text.replace(' ', '').replace('\n', '').replace('\r', '')
    token = parse_csrf_token_from_content(res_clean)
    socket_url = parse_socket_url_from_content(res_clean)
    user_id = parse_user_id_from_content(res_clean)
    if res.status_code == 200:
        return [token, cookies, socket_url, user_id]
    else:
        return None


def get_socket_token(user_token, csrf_token, cookies):
    url = 'http://widget.donatepay.ru/socket/token'
    payload = {
        'token': user_token,
        '_token': csrf_token
       }
    cookies_parsed = parse_cookies_dict_to_string(cookies)
    headers = {
        'cookie': cookies_parsed,
        'pragma': "no-cache",
        'origin': "http://widget.donatepay.ru",
        'accept-encoding': "gzip, deflate",
        'host': "widget.donatepay.ru",
        'accept-language': "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,und;q=0.6",
        'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
        'content-type': "application/x-www-form-urlencoded",
        'accept': "*/*",
        'cache-control': "no-cache",
        'x-requested-with': "XMLHttpRequest",
        'connection': "keep-alive",
        'referer': "http://widget.donatepay.ru/player/widget/" + user_token,
        'content-length': "118"
       }
    res = requests.post(url, data=payload, headers=headers)
    token = json.loads(res.text)
    if res.status_code == 200:
        return token
    else:
        return None
