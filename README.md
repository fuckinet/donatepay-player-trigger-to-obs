## Obs scene remote change by socket io event

#### Before first application start, create python design file
`pyuic5 lib/design.ui -o lib/design.py`

\* you need to run the command after each design.ui file update 

#### Start application
`python main.py`

#### Build project to single file in *nix system
`./build_unix.sh`

##### for windows system
`./build_windows.bat`

##### For OBS, user should install `obs-websocket` plugin
https://github.com/Palakis/obs-websocket

#### Requirements
* python 3.7
* python pip
* pip `obs-websocket-py` module
* pip `websockets` module
* pip `centrifuge` module
* pip `requests` module
* pip `quamash` module
* pip `pyinstaller` module

##### * install requirements
pip install obs-websocket-py websockets centrifuge requests quamash pyinstaller
