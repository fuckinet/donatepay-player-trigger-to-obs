@echo off

:: Define directories
set SCRIPT_DIR=%CD%
set LIB_DIR=%SCRIPT_DIR%\lib
set DIST_DIR=%SCRIPT_DIR%\dist
set BUILD_DIR=%SCRIPT_DIR%\build

:: Clean dist & cache before build
rmdir /s /q "%DIST_DIR%"
rmdir /s /q "%BUILD_DIR%"
del /s /q "%SCRIPT_DIR%\DonatePayMusicToObs.spec"

:: Build design
pyuic5 %LIB_DIR%\design.ui -o %LIB_DIR%\design.py

:: Build application to a single file
pyinstaller --clean --onefile ^
    --windowed --noconsole ^
    --name="DonatePayMusicToObs" ^
    --icon="%LIB_DIR%\icon.ico" ^
    --add-data="%LIB_DIR%\icon.ico;lib" ^
    main.py

:: Copy config file to dist directory
copy "%SCRIPT_DIR%\config.json" %DIST_DIR%

:: Clear icon cache
ie4uinit.exe -ClearIconCache
:: w10
ie4uinit.exe -show
