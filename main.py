import time
import asyncio
import utils
import obs_remote
import obswebsocket
import sys
import os
import threading
import lib.design as design
from requests.exceptions import ConnectionError
from centrifuge import Client, Credentials
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtCore import QThread, pyqtSignal
from quamash import QEventLoop
from time import localtime, strftime

if getattr(sys, 'frozen', False):
    app_path = sys._MEIPASS
else:
    app_path = os.path.dirname(os.path.abspath(__file__))
config_data = utils.parse_config_file()

track_play_now = False


def set_track_play_now_to_true():
    global track_play_now
    track_play_now = True


def set_track_play_now_to_false():
    global track_play_now
    track_play_now = False


def get_track_play_now():
    return track_play_now


@asyncio.coroutine
async def run(parent):
    user_token = config_data.get('token')
    try:
        [csrf_token, cookies, socket_url, user_id]\
            = utils.get_csrf_token_and_cookies_and_socket_url(user_token)
        if csrf_token is None:
            parent.log_to_app('Произошла ошибки при получении CSRF токена!', 'error')
            print('Unable to get CSRF token')
        else:
            parent.log_to_app('CSRF токен успешно получен.', 'info')
    except ConnectionError:
        parent.log_to_app('Произошла ошибки при получении CSRF токена!', 'error')
    if cookies is None:
        parent.log_to_app('Произошла ошибки при получении печенек!', 'error')
        print('Unable to get cookies')
    else:
        parent.log_to_app('Куки успешно получены.', 'info')
    if socket_url is None:
        print('Unable to get socket url')
        parent.log_to_app('Произошла ошибки при получении адреса сокета сервера', 'error')
    else:
        parent.log_to_app('Адрес сокет сервера успешно получен.', 'info')
    if user_id is None:
        print('Unable to get socket url')
        parent.log_to_app('Произошла ошибки при получении id пользователя', 'error')
    else:
        parent.log_to_app('Id пользователя успешно получен.', 'info')
    time.sleep(0.5)
    socket_token = utils.get_socket_token(user_token, csrf_token, cookies)
    if socket_token.get('token') is None:
        parent.log_to_app('Произошла ошибки при получении авторизационного токена для сокета!', 'error')
    else:
        parent.log_to_app('Авторизационный сокет токен успешно получен.', 'info')
    # connect to obs remote scene
    obs = obs_remote.ObsRemote(config_data.get('obs').get('host'),
                               config_data.get('obs').get('port'),
                               config_data.get('obs').get('password'))
    try:
        obs.connect()
        parent.log_to_app('Успешное подключение к OBS.', 'info')
    except obswebsocket.core.exceptions.ConnectionFailure:
        parent.log_to_app('Произошла ошибка при подключении к OBS!', 'error')
        print('Unable to connect to obs')

    user = str(user_id)
    timestamp = str(socket_token.get('time'))
    info = ''
    token = str(socket_token.get('token'))

    credentials = Credentials(user, timestamp, info, token)
    address = 'ws://' + socket_url.get('host') + ':' + socket_url.get('port')\
        + '/connection/websocket'

    async def connect_handler(**kwargs):
        parent.log_to_app('Успешное подключение к сервису DonatePay.', 'success')
        print("Connected", kwargs)

    async def disconnect_handler(**kwargs):
        parent.log_to_app('Ошибка при подключении к DonatePaySocket: Disconnected', 'error')
        print("Disconnected:", kwargs)

    async def connect_error_handler(**kwargs):
        parent.log_to_app('Ошибка при подключении к DonatePaySocket: ConnectError', 'error')
        print("Error:", kwargs)

    client = Client(
        address, credentials,
        on_connect=connect_handler,
        on_disconnect=disconnect_handler,
        on_error=connect_error_handler
    )

    await client.connect()

    async def message_handler(**kwargs):
        action = kwargs.get('data').get('action')
        if action == 'player.start':
            set_track_play_now_to_true()
            print('Need to change obs scene to scene with music')
            obs.switch_to_scene(config_data.get('scenes').get('onPlayerStart'))
        elif action == 'player.end':
            set_track_play_now_to_false()
            print('Stop music event: change scene to spotify')
            wait_seconds = int(config_data.get('timeoutAfterTrackEnd'))
            if wait_seconds >= 1:
                def wait_before_end():
                    print('wait ' + str(wait_seconds) + ' before change scene')
                    time.sleep(wait_seconds)
                    if get_track_play_now() is False:
                        obs.switch_to_scene(config_data.get('scenes').get('onPlayerEnd'))
                        print('scene changed after wait')
                    else:
                        print('scene not changed after wait')
                wait_thread = threading.Thread(target=wait_before_end)
                wait_thread.start()
            else:
                set_track_play_now_to_false()
                obs.switch_to_scene(config_data.get('scenes').get('onPlayerEnd'))
                print('scene changed')
        else:
            print('unknown action response:', kwargs)

    async def join_handler(**kwargs):
        print("Join:", kwargs)

    async def leave_handler(**kwargs):
        print("Leave:", kwargs)

    async def error_handler(**kwargs):
        print("Sub error:", kwargs)

    await client.subscribe(
        'events:events#' + user,
        on_message=message_handler,
        on_join=join_handler,
        on_leave=leave_handler,
        on_error=error_handler
    )


class ApplicationController(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon(os.path.join(app_path, 'lib/icon.ico')))

        self.get_thread = CentrifugeThread(self)
        self.get_thread.log_to_app_.connect(self.log_to_app)
        self.get_thread.start()

    def log_to_app(self, message, mes_type='black'):
        color = 'black'
        if mes_type == 'success':
            color = 'green'
        elif mes_type == 'error':
            color = 'darkred'
        template = '[' + str(strftime("%H:%M:%S", localtime())) + '] '\
                   '<span style="color:' + color + '" >'\
                   + str(message)\
                   + '</span><br/><br/>'
        self.applicationLogger.insertHtml(str(template))
        self.applicationLogger.moveCursor(QtGui.QTextCursor.End)
        print(template.encode(sys.stdout.encoding, errors='replace'))


class CentrifugeThread(QThread):
    log_to_app_ = pyqtSignal(str, str)

    def __init__(self, parent):
        QThread.__init__(self)
        self.parent = parent

    def __del__(self):
        self.wait()

    def log_to_app(self, *kwargs):
        self.log_to_app_.emit(*kwargs)

    def run(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        asyncio.ensure_future(run(self), loop=loop)
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            print("interrupted")
        finally:
            loop.close()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)
    window = ApplicationController()
    window.show()
    sys.exit(app.exec_())
